package Models;

public class Station
{
    private int state;

    private int position;

    private String name;

    public Station(int state, int position, String name)
    {
        this.state = state;
        this.position = position;
        this.name = name;
    }

    private boolean open()
    {
        return true;
    }

    private boolean close()
    {
        return true;
    }

    public int checkState()
    {
        return 1;
    }
}