package Models;

import java.util.HashMap;
import java.util.Map;

public class Notice {

    private int state;

    //private Map<Integer, String> states = new HashMap<Integer, String>;

    private String name;

    private int current;

    public Notice(int state, String name, int current) {
        this.state = state;
        this.name = name;
        this.current = current;

        //this.states.put(1, "open");
        //this.states.put(2, "close");
    }

    public int run() {
        return 1;
    }

    public int stop() {
        return 1;
    }

    public int next() {
        return 1;
    }

    public int previos() {
        return 1;
    }

    public int setCurrent(int current) {
        return this.current = current;
    }

    public String setName(String name) {
        return this.name = name;
    }
}