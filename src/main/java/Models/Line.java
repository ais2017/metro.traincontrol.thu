package Models;

import java.util.Collection;

public class Line
{
    private String name;

    private Collection<Train> trains;

    private Collection<Station> stations;

    private Collection<Service> services;

    public Line(String name)
    {
        this.name = name;
    }

    public int addStation(int state, int position, String name)
    {
        return 1;
    }

    public int addService(int state, int type, int position)
    {
        return 1;
    }

    public int addTrain(int state, int position)
    {
        return 1;
    }

    public int deleteTrain(Train train)
    {
        return 1;
    }

    public int getTrainsPosition()
    {
        return 1;
    }

    private int sendWarning(String message)
    {
        return 1;
    }

    public int checkTrainPosition(Train train)
    {
        return 1;
    }

    public int sendTrainToService(Train train, Service service)
    {
        return 1;
    }

    public int stopTrain(Train train)
    {
        return 1;
    }
}