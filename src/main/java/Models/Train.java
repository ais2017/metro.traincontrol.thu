package Models;

import java.util.Collection;

public class Train {

    private int state;

    private float speed;

    private int position;

    private Collection<Door> doors;

    private Notice notice;

    private int wayDirection;

    public Train(int state, int position) {
        this.state = state;
        this.position = position;
    }

    private boolean changeState(int state) {
        return true;
    }

    private int checkState() {
        return 1;
    }

    private float speedUp(float speed) {
        return 1;
    }

    private float speedDown(float speed) {
        return 1;
    }

    private int openDoors(Collection<Door> doors) {
        return 1;
    }

    private int closeDoors(Collection<Door> doors) {
        return 1;
    }

    private int callNotice(String name) {
        return 1;
    }

    public int checkPosition() {
        return 1;
    }

    private int changeWayDirection(int wayDirection) {
        return 1;
    }
}