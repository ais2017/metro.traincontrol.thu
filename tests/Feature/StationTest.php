<?php

namespace Tests\Feature;

use App\Station;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StationTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex()
    {
        $response = $this->get('station');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $station = factory(Station::class)->make();

        $response = $this->post('station', $station->toArray());

        $response->assertStatus(200);

        $this->assertDatabaseHas('stations', ['name' => $station->name]);
    }

    public function testShow()
    {
        $station = factory(Station::class)->create();

        $response = $this->get('station/' . $station->id);

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $station = factory(Station::class)->create();

        $newStation = factory(Station::class)->make();

        $response = $this->put('station/' . $station->id, $newStation->toArray());

        $response->assertStatus(200);
    }

    public function testDestroy()
    {
        $station = factory(Station::class)->create();

        $response = $this->delete('station/' . $station->id);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('stations', ['id' => $station->id]);
    }

    public function testState()
    {
        $station = factory(Station::class)->create();

        $response = $this->get('station/' . $station->id . '/state');

        $response->assertStatus(200);
    }
}
