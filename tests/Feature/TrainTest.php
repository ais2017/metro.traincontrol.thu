<?php

namespace Tests\Feature;

use App\Train;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrainTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex()
    {
        $response = $this->get('train');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $train = factory(Train::class)->make();

        $response = $this->post('train', $train->toArray());

        $response->assertStatus(200);

        $this->assertDatabaseHas('trains', ['position' => $train->position]);
    }

    public function testShow()
    {
        $train = factory(Train::class)->create();

        $response = $this->get('train/' . $train->id);

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $train = factory(Train::class)->create();

        $newTrain = factory(Train::class)->make();

        $response = $this->put('train/' . $train->id, $newTrain->toArray());

        $response->assertStatus(200);
    }

    public function testState()
    {
        $train = factory(Train::class)->create();

        $response = $this->get('train/' . $train->id . '/state');

        $response->assertStatus(200);
    }

    public function testPosition()
    {
        $train = factory(Train::class)->create();

        $response = $this->get('train/' . $train->id . '/position');

        $response->assertStatus(200);
    }

    public function testOpenDoors()
    {
        $train = factory(Train::class)->create();

        $response = $this->get('train/' . $train->id . '/doors/open');

        $response->assertStatus(200);

        $this->assertDatabaseHas('doors', ['state' => 'open']);
    }

    public function testCloseDoors()
    {
        $train = factory(Train::class)->create();

        $response = $this->get('train/' . $train->id . '/doors/close');

        $response->assertStatus(200);

        $this->assertDatabaseHas('doors', ['state' => 'close']);
    }
}
