<?php

namespace Tests\Feature;

use App\Service;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex()
    {
        $response = $this->get('service');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $service = factory(Service::class)->make();

        $response = $this->post('service', $service->toArray());

        $response->assertStatus(200);
    }

    public function testState()
    {
        $service = factory(Service::class)->create();

        $response = $this->get('service/' . $service->id . '/state');

        $response->assertStatus(200);
    }
}
