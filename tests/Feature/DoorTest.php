<?php

namespace Tests\Feature;

use App\Door;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DoorTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex()
    {
        $response = $this->get('door');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $door = factory(Door::class)->make();

        $response = $this->post('door', $door->toArray());

        $response->assertStatus(200);
    }

    public function testShow()
    {
        $door = factory(Door::class)->create();

        $response = $this->get('door/' . $door->id);

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $door = factory(Door::class)->create();

        $newDoor = factory(Door::class)->make();

        $response = $this->put('door/' . $door->id, $newDoor->toArray());

        $response->assertStatus(200);
    }
}
