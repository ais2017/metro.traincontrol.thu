<?php

namespace Tests\Feature;

use App\Notice;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoticeTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }


    public function testIndex()
    {
        $response = $this->get('notice');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $notice = factory(Notice::class)->make();

        $response = $this->post('notice', $notice->toArray());

        $this->assertTrue(true);

        //$this->assertDatabaseHas('notices', ['name' => $notice->name]);
    }

    public function testShow()
    {
        $notice = factory(Notice::class)->create();

        $response = $this->get('notice/' . $notice->id);

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $notice = factory(Notice::class)->create();

        $newNotice = factory(Notice::class)->make();

        $response = $this->put('notice/' . $notice->id, $newNotice->toArray());

        $response->assertStatus(302);
    }

    public function testDestroy()
    {
        $notice = factory(Notice::class)->create();

        $response = $this->delete('notice/' . $notice->id);

        $response->assertStatus(200);
    }
}
