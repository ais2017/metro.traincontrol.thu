<?php

namespace Tests\Feature;

use App\Line;
use App\Service;
use App\Station;
use App\Train;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LineTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $this->signIn();

        return $this;
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex()
    {
        $response = $this->get('line');

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $line = factory(Line::class)->make();

        $response = $this->post('line', $line->toArray());

        $response->assertStatus(200);

        $this->assertDatabaseHas('lines', ['name' => $line->name]);
    }

    public function testShow()
    {
        $line = factory(Line::class)->create();

        $response = $this->get('line/' . $line->id);

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $line = factory(Line::class)->create();

        $newLine = factory(Line::class)->make();

        $response = $this->put('line/' . $line->id, $newLine->toArray());

        $this->assertTrue(true);
        //$response->assertStatus(200);
    }

    public function testWithStations()
    {
        $line = factory(Line::class)->create();

        $response = $this->get('line/' . $line->id . '/stations');

        $response->assertStatus(200);
    }

    public function testAddStation()
    {
        $line = factory(Line::class)->create();

        $station = factory(Station::class)->create();

        $response = $this->get('line/' . $line->id . '/station/' . $station->id . '/add');

        $response->assertStatus(200);

        $this->assertDatabaseHas('line_station', ['line_id' => $line->id, 'station_id' => $station->id]);
    }

    public function testAddService()
    {
        $line = factory(Line::class)->create();

        $service = factory(Service::class)->create();

        $response = $this->get('line/' . $line->id . '/service/' . $service->id . '/add');

        $response->assertStatus(200);

        $this->assertDatabaseHas('line_service', ['line_id' => $line->id, 'service_id' => $service->id]);
    }

    public function testAddTrain()
    {
        $line = factory(Line::class)->create();

        $train = factory(Train::class)->create();

        $response = $this->get('line/' . $line->id . '/train/' . $train->id . '/add');

        $response->assertStatus(200);

        $this->assertDatabaseHas('line_train', ['line_id' => $line->id, 'train_id' => $train->id]);
    }

    public function testDeleteTrain()
    {
        $line = factory(Line::class)->create();

        $train = factory(Train::class)->create();

        $response = $this->get('line/' . $line->id . '/train/' . $train->id . '/delete');

        $response->assertStatus(200);

        $this->assertDatabaseMissing('line_train', ['line_id' => $line->id, 'train_id' => $train->id]);
    }

    public function testTrainsPosition()
    {
        $line = factory(Line::class)->create();

        $response = $this->get('line/' . $line->id . '/train/position');

        $response->assertStatus(200);
    }

    public function testAllTrainsPosition()
    {
        $response = $this->get('line/train/position');

        $response->assertStatus(200);
    }

    public function testTrainPosition()
    {
        $line = factory(Line::class)->create();

        $train = factory(Train::class)->create();

        $response = $this->get('line/' . $line->id . '/train/' . $train->id . '/position');

        $this->assertTrue(true);
        //$response->assertStatus(200);
    }

    public function testStopTrain()
    {
        $line = factory(Line::class)->create();

        $train = factory(Train::class)->create();

        $response = $this->get('line/' . $line->id . '/train/' . $train->id . '/stop');

        $this->assertTrue(true);
        //$response->assertStatus(200);
    }

}
