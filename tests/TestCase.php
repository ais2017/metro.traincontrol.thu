<?php

namespace Tests;

use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Symfony\Component\Debug\ExceptionHandler;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();
        $this->disableExceptionHandling();
    }
    protected function signIn($roleIds = [1,2])
    {
        //Session::start();
        //$user = factory(User::class)->create();
        //$user->roles()->attach($roleIds);
        //$this->actingAs($user);
        return $this;
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}
            public function report(\Exception $e) {}
            public function render($request, \Exception $e) {
                throw $e;
            }
        });
    }
    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);
        return $this;
    }

    public function __call($method, $args)
    {
        if (in_array($method, ['get', 'post', 'put', 'patch', 'delete']))
        {
            return $this->call($method, $args[0]);
        }

        throw new BadMethodCallException;
    }

    protected function withToken($params = null)
    {
        $token = ['_token' => csrf_token()];

        if (!$params) {
            $result = $token;
        } elseif (is_array($params)) {
            $result = array_merge($params, $token);
        } else {
            $result = array_merge($params->toArray(), $token);
        }

        return $result;
    }
}
