
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Тестер</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style>
        /* Syntax highlighting for JSON objects */
        ul.json-dict, ol.json-array {
            list-style-type: none;
            margin: 0 0 0 1px;
            border-left: 1px dotted #ccc;
            padding-left: 2em;
        }
        .json-string {
            color: #0B7500;
        }
        .json-literal {
            color: #1A01CC;
            font-weight: bold;
        }

        /* Toggle button */
        a.json-toggle {
            position: relative;
            color: inherit;
            text-decoration: none;
        }
        a.json-toggle:focus {
            outline: none;
        }
        a.json-toggle:before {
            color: #aaa;
            content: "\25BC"; /* down arrow */
            position: absolute;
            display: inline-block;
            width: 1em;
            left: -1em;
        }
        a.json-toggle.collapsed:before {
            content: "\25B6"; /* left arrow */
        }

        /* Collapsable placeholder links */
        a.json-placeholder {
            color: #aaa;
            padding: 0 1em;
            text-decoration: none;
        }
        a.json-placeholder:hover {
            text-decoration: underline;
        }
    </style>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/js/dpong.js"></script>
    <script>
        /**
         * jQuery json-viewer
         * @author: Alexandre Bodelot <alexandre.bodelot@gmail.com>
         */
        (function($){

            /**
             * Check if arg is either an array with at least 1 element, or a dict with at least 1 key
             * @return boolean
             */
            function isCollapsable(arg) {
                return arg instanceof Object && Object.keys(arg).length > 0;
            }

            /**
             * Check if a string represents a valid url
             * @return boolean
             */
            function isUrl(string) {
                var regexp = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
                return regexp.test(string);
            }

            /**
             * Transform a json object into html representation
             * @return string
             */
            function json2html(json, options) {
                var html = '';
                if (typeof json === 'string') {
                    // Escape tags
                    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    if (isUrl(json))
                        html += '<a href="' + json + '" class="json-string">' + json + '</a>';
                    else
                        html += '<span class="json-string">"' + json + '"</span>';
                }
                else if (typeof json === 'number') {
                    html += '<span class="json-literal">' + json + '</span>';
                }
                else if (typeof json === 'boolean') {
                    html += '<span class="json-literal">' + json + '</span>';
                }
                else if (json === null) {
                    html += '<span class="json-literal">null</span>';
                }
                else if (json instanceof Array) {
                    if (json.length > 0) {
                        html += '[<ol class="json-array">';
                        for (var i = 0; i < json.length; ++i) {
                            html += '<li>'
                            // Add toggle button if item is collapsable
                            if (isCollapsable(json[i])) {
                                html += '<a href class="json-toggle"></a>';
                            }
                            html += json2html(json[i], options);
                            // Add comma if item is not last
                            if (i < json.length - 1) {
                                html += ',';
                            }
                            html += '</li>';
                        }
                        html += '</ol>]';
                    }
                    else {
                        html += '[]';
                    }
                }
                else if (typeof json === 'object') {
                    var key_count = Object.keys(json).length;
                    if (key_count > 0) {
                        html += '{<ul class="json-dict">';
                        for (var key in json) {
                            if (json.hasOwnProperty(key)) {
                                html += '<li>';
                                var keyRepr = options.withQuotes ?
                                    '<span class="json-string">"' + key + '"</span>' : key;
                                // Add toggle button if item is collapsable
                                if (isCollapsable(json[key])) {
                                    html += '<a href class="json-toggle">' + keyRepr + '</a>';
                                }
                                else {
                                    html += keyRepr;
                                }
                                html += ': ' + json2html(json[key], options);
                                // Add comma if item is not last
                                if (--key_count > 0)
                                    html += ',';
                                html += '</li>';
                            }
                        }
                        html += '</ul>}';
                    }
                    else {
                        html += '{}';
                    }
                }
                return html;
            }

            /**
             * jQuery plugin method
             * @param json: a javascript object
             * @param options: an optional options hash
             */
            $.fn.jsonViewer = function(json, options) {
                options = options || {};

                // jQuery chaining
                return this.each(function() {

                    // Transform to HTML
                    var html = json2html(json, options)
                    if (isCollapsable(json))
                        html = '<a href class="json-toggle"></a>' + html;

                    // Insert HTML in target DOM element
                    $(this).html(html);

                    // Bind click on toggle buttons
                    $(this).off('click');
                    $(this).on('click', 'a.json-toggle', function() {
                        var target = $(this).toggleClass('collapsed').siblings('ul.json-dict, ol.json-array');
                        target.toggle();
                        if (target.is(':visible')) {
                            target.siblings('.json-placeholder').remove();
                        }
                        else {
                            var count = target.children('li').length;
                            var placeholder = count + (count > 1 ? ' items' : ' item');
                            target.after('<a href class="json-placeholder">' + placeholder + '</a>');
                        }
                        return false;
                    });

                    // Simulate click on toggle button when placeholder is clicked
                    $(this).on('click', 'a.json-placeholder', function() {
                        $(this).siblings('a.json-toggle').click();
                        return false;
                    });

                    if (options.collapsed == true) {
                        // Trigger click to collapse all nodes
                        $(this).find('a.json-toggle').click();
                    }
                });
            };
        })(jQuery);
    </script>
</head>
<body>
<div class="row">
    <div class="col-md-6">
        <div class="col-md-4">
            <form method="post" action="/images" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image">
                    <input type="text" class="form-control" name="alt" id="alt" placeholder="alt">
                </div>
                <button type="submit" class="btn btn-default">Upload</button>
            </form>
        </div>
        <div class="col-md-4">
            <form method="post" action="videos" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="video">Video</label>
                    <input type="file" name="video" id="video">
                    <input type="text" class="form-control" name="title" id="title" placeholder="title">
                    <input type="text" class="form-control" name="description" id="description" placeholder="description">
                    <input type="text" class="form-control" name="status" id="status" placeholder="status">
                </div>
                <button type="submit" class="btn btn-default">Upload</button>
            </form>
        </div>

        <div class="col-md-4">
            <form method="post" action="/youtube/videos" enctype="multipart/form-data" class="form-inline">
                <div class="form-group">
                    {{ csrf_field() }}
                    <label for="video">Youtube</label>
                    <input type="file" name="video" id="video">
                    <label for="thumbnail">Thumbnail</label>
                    <input type="file" name="thumbnail" id="thumbnail">
                    <input type="text" class="form-control" name="options" id="options" placeholder="options">
                    <input type="text" class="form-control" name="slug" id="slug" placeholder="slug">
                </div>
                <button type="submit" class="btn btn-default">Upload</button>
            </form>
        </div>
        <form role="form">
            <div class="form-group">
                <label for="route">Route::post</label>
                <input type="text" class="form-control" id="route" placeholder="tester">
                <select name="method" id="method">
                    <option value="POST">post</option>
                    <option value="GET">get</option>
                    <option value="PUT">put</option>
                    <option value="DELETE">delete</option>
                </select>
            </div>
            <textarea class="form-control" id="json" rows="12">{

                }</textarea>
            <button type="button" id="send" class="btn btn-default">Запустить тест</button>
        </form>
    </div>
    <div class="col-md-6">
        <div id="success"></div>
        <div id="result">

        </div>
    </div>
</div>
<script>
    $('#send').click(function () {
        let route = $('#route').val();
        let json = JSON.parse($('#json').val());
        dpong.server.post(route,json,function (data) {
            $('#result').jsonViewer(data);

        },$('#method').val());
    })
</script>
</body>
</html></script>