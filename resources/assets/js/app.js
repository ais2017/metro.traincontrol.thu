
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.use(VueResource)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('navigation', require('./components/NavigationComponent.vue'));
Vue.component('all-trains', require('./components/AllTrainsComponent.vue'));
Vue.component('line-trains', require('./components/LineTrainsComponent.vue'));
Vue.component('train', require('./components/TrainComponent.vue'));


import Metro from './pages/Metro.vue'
import Line from './pages/Line.vue'
import Train from './pages/Train.vue'

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/', component: Metro },
        { path: '/line', component: Line },
        { path: '/train', component: Train }
    ]
})

const app = new Vue({
    router
}).$mount('#app');
