$(document).ready(function() {
    dpong.init();

});

//форма PHP
dpong = {


//Сохранение при нажатие на Enter
    enterSave: function(focusId,func){
        $(focusId).focus(function(){
            $(focusId).keypress(function(e){
                if(e.keyCode==13){
                    var val = $(focusId).val();
                    func(val,focusId);
                }
            });
        });

        $(focusId).blur(function(){
            var val = $(focusId).val();
            func(val,focusId);
        });
    },

//Сохранение при изменении
    changeSave: function(focusId,func){
        $(focusId).change(function(){
            var val = $(focusId).val();
            func(val);
        });
    },


    server:{


        router:"",

        //request: null,

        post:function (action,post,func,method) {

            method = method ? method : "POST";

            /*
            if (!post){
               post={action:action};
            } else {
                post.action = action;
            }

            var args = "";
            for (val in post) {
                if (args) args+="&";
                if (typeof(post[val]) === 'object') { post[val] = JSON.stringify(post[val]) }
                args += val+"="+post[val];
            }
            */

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //alert($('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                type: method,
                contentType: "application/json",
                dataType: "json",
                url: "/"+action,
                data: JSON.stringify(post),
                //data: args,
                //
                success: function(msg){
                    func(msg);
                }
            });

        },



        files:function (files,action,data,func) {

            var form = new FormData();
            var arr = document.getElementById(files);

            if (arr.files.length > 1 ) {
                for (var i = 0; i < arr.files.length; i++) {
                    form.append($("#"+files).prop('name') + i, arr.files[i]);
                }
            } else {
                form.append($("#"+files).prop('name'), arr.files[0]);
            }

            //form.append("action", action);

            for (varb in data) {
                form.append(varb, data[varb]);
            }

            var xhr = dpong.server.getXmlHttp();
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    func(xhr.responseText);
                }
            }
            xhr.open("post", action, true);
            xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            xhr.send(form);
        },

        getXmlHttp: function(){
            var xmlhttp;
            try{
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e){
                try {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E){
                    xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest!='undefined'){
                xmlhttp = new XMLHttpRequest();
            }
            return xmlhttp;
        },



    },


    init:function () {
        $(".dp-submit").click(function () {
            var value = $(this).data("value");
            var group = $(this).data("group");
            var result = $(this).data("result");
            var processor = $(this).data("processor");
            var preprocessor = $(this).data("preprocessor");
            var action = $(this).data("action") || null;

            if (preprocessor) {
                dpong[preprocessor]("#" + result);
            }

            //post = dpong.server.post(action,value,group);
            //$("#"+result).html(post);

            var post = {action: action};
            $("." + group).each(function () {
                post[$(this).attr('name')] = $(this).val();
            });

            if (value) {
                post["value"] = value;
            }

            $.post(dpong.server.router, post)
                .done(function (data) {
                    var parseData = data.split(':::');
                    if (parseData[1]) {
                        var nameFunc = parseData[0].trim();
                        processResult = dpong[nameFunc]("#" + result, parseData[1]);
                        $("#" + result).html(processResult);
                    } else {
                        $("#" + result).html(parseData[0]);
                    }
                })
                .fail(function () {
                    alert("Ошибка асинронного запроса");
                });

            if (processor) {
                dpong[processor]("#" + result);
            }
        });

//Форма JS
        $(".dp-button").click(function () {
            var group = $(this).data("group");
            var value = $(this).data("value");
            var result = $(this).data("result");
            var processor = $(this).data("processor");


            if (group) {
                value = {};
                value['value'] = $(this).data("value");
                $("." + group).each(function () {
                    value[$(this).attr('name')] = $(this).val();
                });
            }
            dpong[processor]("#" + result, value);
        });
    }
}