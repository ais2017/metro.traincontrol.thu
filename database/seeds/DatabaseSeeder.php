<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LineSeeder::class);
        $this->call(NoticeSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(StationSeeder::class);
        $this->call(TrainSeeder::class);
    }
}
