<?php

use App\Door;
use App\Notice;
use App\Train;
use Illuminate\Database\Seeder;

class TrainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Train::class, rand(40, 100))->create()
            ->each(function($t) {
                $t->doors()->saveMany(factory(Door::class, 20)->make());
                $t->notices()->saveMany(factory(Notice::class, 20)->make());
            });
    }
}
