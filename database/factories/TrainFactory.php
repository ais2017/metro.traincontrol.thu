<?php

use App\Train;
use Faker\Generator as Faker;

$factory->define(Train::class, function (Faker $faker) {
    return [
        'state' => $faker->randomElement(['run', 'stop']),
        'speed' => rand(0, 100),
        'position' => $faker->unique()->randomNumber(),
        'way_direction' => $faker->randomElement(['left', 'right']),
    ];
});
