<?php

use App\Door;
use Faker\Generator as Faker;

$factory->define(Door::class, function (Faker $faker) {
    return [
        'train_id' => factory(\App\Train::class)->create()->id,
        'state' => $faker->randomElement(['open', 'close']),
        'side' => $faker->randomElement(['left', 'right']),
    ];
});
