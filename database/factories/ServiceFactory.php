<?php

use App\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord,
        'state' => $faker->randomElement(['open', 'close']),
        'type' => $faker->randomElement(['a', 'b', 'c']),
        'position' => $faker->unique()->randomNumber(),
    ];
});
