<?php

use App\Notice;
use Faker\Generator as Faker;

$factory->define(Notice::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord,
    ];
});
