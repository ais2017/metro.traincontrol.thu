<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainNoticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('train_notice', function (Blueprint $table) {
            $table->unsignedInteger('train_id');
            $table->unsignedInteger('notice_id');
            $table->boolean('current')->default(false);
            $table->timestamps();

            $table->foreign('train_id')->references('id')->on('trains');
            $table->foreign('notice_id')->references('id')->on('notices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('train_notices');
    }
}
