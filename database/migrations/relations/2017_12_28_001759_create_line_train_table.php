<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineTrainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_train', function (Blueprint $table) {
            $table->unsignedInteger('line_id');
            $table->unsignedInteger('train_id');
            $table->timestamps();

            $table->unique(['line_id', 'train_id']);
            $table->foreign('line_id')->references('id')->on('lines');
            $table->foreign('train_id')->references('id')->on('trains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_trains');
    }
}
