<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Train
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Door[] $doors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Line[] $line
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Notice[] $notices
 * @mixin \Eloquent
 * @property int $id
 * @property string $state
 * @property float $speed
 * @property int $position
 * @property string $way_direction
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Train whereWayDirection($value)
 */
class Train extends Model
{
    protected $fillable = [
        'state',
        'speed',
        'position',
        'notice',
        'way_direction',
    ];

    public function doors(): HasMany
    {
        return $this->hasMany(Door::class);
    }

    public function notices(): BelongsToMany
    {
        return $this->belongsToMany(Notice::class, 'train_notice');
    }

    public function line(): BelongsToMany
    {
        return $this->belongsToMany(Line::class);
    }
}
