<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Line
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Station[] $stations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Train[] $trains
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereUpdatedAt($value)
 */
class Line extends Model
{
    protected $fillable = [
        'name',
    ];

    public function trains(): BelongsToMany
    {
        return $this->belongsToMany(Train::class);
    }

    public function stations(): BelongsToMany
    {
        return $this->belongsToMany(Station::class);
    }

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class);
    }
}
