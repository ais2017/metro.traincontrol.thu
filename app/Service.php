<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Service
 *
 * @property-read \App\Line $line
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $state
 * @property string $type
 * @property int $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 */
class Service extends Model
{

    protected $fillable = [
        'name',
        'state',
        'type',
        'position',
    ];

    public function line(): BelongsToMany
    {
        return $this->belongsToMany(Line::class);
    }
}
