<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Station
 *
 * @property-read \App\Line $line
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $state
 * @property int $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereUpdatedAt($value)
 */
class Station extends Model
{

    protected $fillable = [
        'name',
        'state',
        'position',
    ];

    public function line(): BelongsToMany
    {
        return $this->belongsToMany(Line::class);
    }
}
