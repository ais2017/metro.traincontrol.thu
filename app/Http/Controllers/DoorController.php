<?php

namespace App\Http\Controllers;

use App\Door;
use App\Http\Requests\Door\StoreDoorRequest;
use App\Http\Requests\Door\UpdateDoorRequest;
use App\Http\Requests\SendWarningRequest;
use Illuminate\Http\Request;

class DoorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Door[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Door::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Door[]|\Illuminate\Database\Eloquent\Collection
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDoorRequest|Request $request
     * @return Door|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreDoorRequest $request)
    {
        $door = Door::create($request->all());

        return $door;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Door  $door
     * @return Door
     */
    public function show(Door $door)
    {
        return $door;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function edit(Door $door)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDoorRequest|Request $request
     * @param  \App\Door $door
     * @return Door
     */
    public function update(UpdateDoorRequest $request, Door $door)
    {
        $door->update($request->all());

        return $door;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function destroy(Door $door)
    {
        //
    }

    public function sendWarning(SendWarningRequest $request, Door $door)
    {
        $title = $request->input('title');
        $content = $request->input('content');

        $content = $content . 'Line ' . $door->id . ' warning!';

        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('me@gmail.com', 'Roman Kolosov');

            $message->to('rvkolosov@scotch.io');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
