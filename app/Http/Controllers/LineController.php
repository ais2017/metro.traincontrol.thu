<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendWarningRequest;
use App\Http\Resources\LineTrainCollection;
use App\Http\Resources\LineTrainResource;
use App\Line;
use App\Service;
use App\Station;
use App\Train;
use Illuminate\Http\Request;
use Mail;

class LineController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Line[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $lines = Line::all();

        return compact('lines');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Line|\Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request)
    {
        return Line::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Line  $line
     * @return Line
     */
    public function show(Line $line)
    {
        return $line;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function edit(Line $line)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Line  $line
     * @return bool
     */
    public function update(Request $request, Line $line)
    {
        return $line->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Line  $line
     * @return \Illuminate\Http\Response
     */
    public function destroy(Line $line)
    {
        //
    }

    public function withStations(Line $line)
    {
        return $line->load('stations');
    }

    public function addStation(Line $line, Station $station)
    {
        $line->stations()->attach($station);

        return $line->load('stations');
    }

    public function addService(Line $line, Service $service)
    {
        $line->services()->attach($service);

        return $line->load('services');
    }

    public function addTrain(Line $line, Train $train)
    {
        $line->trains()->attach($train);

        return $line->load('trains');
    }

    public function deleteTrain(Line $line, Train $train)
    {
        $line->trains()->detach($train);

        return $line->load('trains');
    }

    public function trainsPosition(Line $line)
    {

        $trains = $line->trains()->get();

        $trainsPosition = $trains->pluck('position', 'id');

        return compact('trainsPosition');

        //return LineTrainResource::collection($line->trains);
    }

    public function allTrainsPosition()
    {
        //$lines = Line::all()->load('trains');

        return new LineTrainCollection(Line::all()->load('trains'));
        //return $lines;
    }

    public function trainPosition(Line $line, Train $train)
    {
        $train->line()->get()->pluck('id')->first() == $line->id ?: abort(404);

        $trainPosition = $train->only('id', 'position');

        return compact('trainPosition');
    }

    public function stopTrain(Line $line, Train $train)
    {
        $train->line()->get()->pluck('id')->first() == $line->id ?: abort(404);

        $train->speed = 0;
        $train->state = 'stop';
        $train->save();

        return $line->trains()->wherePivot('train_id', $train->id)->get();
    }

    public function sendWarning(SendWarningRequest $request, Line $line)
    {
        $title = $request->input('title');
        $content = $request->input('content');

        $content = $content . 'Line ' . $line->id . ' warning!';

        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('me@gmail.com', 'Roman Kolosov');

            $message->to('rvkolosov@scotch.io');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
