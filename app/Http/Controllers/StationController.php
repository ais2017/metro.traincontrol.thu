<?php

namespace App\Http\Controllers;

use App\Http\Requests\Station\StoreStationRequest;
use App\Http\Requests\Station\UpdateStationRequest;
use App\Station;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Station[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Station::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStationRequest|Request $request
     * @return Station|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreStationRequest $request)
    {
        $station = Station::create($request->all());

        return $station;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Station  $station
     * @return Station
     */
    public function show(Station $station)
    {
        return $station;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Station  $station
     * @return Station
     */
    public function edit(Station $station)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStationRequest|Request $request
     * @param  \App\Station $station
     * @return Station
     */
    public function update(UpdateStationRequest $request, Station $station)
    {
        $station->update($request->all());

        return $station;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Station  $station
     * @return Station
     */
    public function destroy(Station $station)
    {
        $station->delete();

        return $station;
    }

    public function state(Station $station)
    {
        $stationState = $station->only('state');

        return compact('stationState');
    }
}
