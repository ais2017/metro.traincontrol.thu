<?php

namespace App\Http\Controllers;

use App\Http\Requests\Station\UpdateStationRequest;
use App\Http\Requests\Train\StoreTrainRequest;
use App\Http\Requests\Train\UpdateTrainRequest;
use App\Http\Resources\TrainResource;
use App\Line;
use App\Notice;
use App\Train;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class TrainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return TrainResource::collection(Train::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTrainRequest|Request $request
     * @return Train|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreTrainRequest $request)
    {
        $train = Train::create($request->all());

        return $train;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Train  $train
     * @return TrainResource
     */
    public function show(Train $train)
    {
        return new TrainResource($train);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Train  $train
     * @return \Illuminate\Http\Response
     */
    public function edit(Train $train)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStationRequest|UpdateTrainRequest|Request $request
     * @param  \App\Train $train
     * @return Train
     */
    public function update(UpdateTrainRequest $request, Train $train)
    {
        $train->update($request->all());

        return $train;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Train  $train
     * @return \Illuminate\Http\Response
     */
    public function destroy(Train $train)
    {
        //
    }

    public function state(Train $train)
    {
        $trainState = $train->only('state');

        return compact('trainState');
    }

    public function position(Train $train)
    {
        $trainPosition = $train->only('position');

        return compact('trainPosition');
    }

    public function openDoors(Train $train)
    {
        $train->doors()->update(['state' => 'open']);

        return $train->doors()->get();
    }

    public function closeDoors(Train $train)
    {
        $train->doors()->update(['state' => 'close']);

        return $train->doors()->get();
    }

    public function notice(Train $train, Notice $notice)
    {
        $calledNotice = $train->notices()->wherePivot('notice_id', $notice->id)->get();

        return compact('calledNotice');
    }
}
