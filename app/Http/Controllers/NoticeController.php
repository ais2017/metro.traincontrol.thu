<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notice\StoreNoticeRequest;
use App\Http\Requests\UpdateNoticeRequest;
use App\Http\Resources\NoticeResource;
use App\Notice;
use DB;
use Illuminate\Http\Request;
use Mockery\Matcher\Not;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Notice[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Notice::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNoticeRequest|Request $request
     * @return Notice|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreNoticeRequest $request)
    {
        $notice = Notice::create($request->all());

        return $notice;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return Notice
     */
    public function show(Notice $notice)
    {
        return $notice;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNoticeRequest|Request $request
     * @param  \App\Notice $notice
     * @return Notice
     */
    public function update(UpdateNoticeRequest $request, Notice $notice)
    {
        $notice->update($request->all());

        return $notice;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return Notice
     */
    public function destroy(Notice $notice)
    {
        $notice->delete();

        return $notice;
    }

    public function test($id, $connection)
    {
        $notice = new Notice();

        $notice->setConnection($connection);

        return new NoticeResource($notice->findOrFail($id));
    }
}
