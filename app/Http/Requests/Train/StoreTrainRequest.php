<?php

namespace App\Http\Requests\Train;

use Illuminate\Foundation\Http\FormRequest;

class StoreTrainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state' => 'required|in:run,stop',
            'speed' => 'required|min:0',
            'position' => 'required|integer|min:0',
            'notice' => 'nullable',
            'way_direction' => 'required|in:left,right',
        ];
    }
}
