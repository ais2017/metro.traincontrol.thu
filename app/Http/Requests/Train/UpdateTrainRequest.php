<?php

namespace App\Http\Requests\Train;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTrainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state' => 'nullable|in:run,stop',
            'speed' => 'nullable|min:0',
            'position' => 'nullable|integer|min:0',
            'notice' => 'nullable',
            'way_direction' => 'nullable|in:left,right',
        ];
    }
}
