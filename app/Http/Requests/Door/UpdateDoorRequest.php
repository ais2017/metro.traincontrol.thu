<?php

namespace App\Http\Requests\Door;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDoorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'train_id' => 'required|exists:trains,id',
            'state' => 'nullable|in:open,close',
            'side' => 'nullable|in:left,right',
        ];
    }
}
