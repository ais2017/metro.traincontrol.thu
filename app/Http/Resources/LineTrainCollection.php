<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LineTrainCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];

        //return parent::toArray($request);
    /*
        return [
            'line_id' => $this->id,
            'line_name' => $this->name,
            'trains' => [
                //'train' => $this->collection()
                /*
                'train_id' => $this->trains->id,
                'train_state' => $this->trains->state,
                'train_position' => $this->trains->position

            ],
        ];
        */

    }
}
