<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Notice
 *
 * @property-read \App\Train $train
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notice whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notice whereUpdatedAt($value)
 */
class Notice extends Model
{
    protected $fillable = [
        'name',
    ];

    //protected $connection = 'mysql2';

    public function train(): BelongsToMany
    {
        return $this->belongsToMany(Train::class, 'train_notice');
    }
}
