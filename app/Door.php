<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Door
 *
 * @property-read \App\Train $train
 * @mixin \Eloquent
 * @property int $id
 * @property string $state
 * @property string $side
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $train_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereSide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereTrainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Door whereUpdatedAt($value)
 */
class Door extends Model
{
    protected $fillable = [
        'train_id',
        'state',
        'side',
    ];

    public function train(): BelongsTo
    {
        return $this->belongsTo(Train::class);
    }
}
