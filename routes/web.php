<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    return view('welcome');
});

Route::get('tester', function () {
    return view('tester.index');
});

Route::resource('train', 'TrainController');

Route::resource('line', 'LineController');

Route::resource('station', 'StationController');

Route::resource('service', 'ServiceController');

Route::resource('notice', 'NoticeController');

Route::resource('door', 'DoorController');

Route::get('line/{line}/stations', 'LineController@withStations');

Route::get('line/{line}/station/{station}/add', 'LineController@addStation');
Route::get('line/{line}/service/{service}/add', 'LineController@addService');
Route::get('line/{line}/train/{train}/add', 'LineController@addTrain');
Route::get('line/{line}/train/{train}/delete', 'LineController@deleteTrain');
Route::get('line/{line}/train/position', 'LineController@trainsPosition');
Route::get('line/{line}/train/{train}/position', 'LineController@trainPosition');
Route::get('line/train/position', 'LineController@allTrainsPosition');
Route::get('line/{line}/train/{train}/stop', 'LineController@stopTrain');
Route::post('line/{line}/send/warning', 'LineController@sendWarning');

Route::get('station/{station}/state', 'StationController@state');

Route::get('service/{service}/state', 'ServiceController@state');

Route::get('train/{train}/state', 'TrainController@state');
Route::get('train/{train}/position', 'TrainController@position');

Route::get('train/{train}/doors/open', 'TrainController@openDoors');
Route::get('train/{train}/doors/close', 'TrainController@closeDoors');

Route::post('door/{door}/send/warning', 'LineController@sendWarning');

Route::get('train/{train}/notice/{notice}/call', 'TrainController@notice');

Route::post('/send', 'EmailController@send')->name('SendWarning');

Route::get('notice/{id}/{connection}', 'NoticeController@test');


